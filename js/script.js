var con = 0;
var key = "";
var div = document.getElementById("four");
var imageUrl = "https://storage.googleapis.com/wineshop-assets/wine-bottles/";
var winedata;
var $wineName;
let cart = [];
let buttonDOM = [];

setTimeout(getWines,3000)

function  getWines(){
    fetch('https://storage.googleapis.com/wineshop-assets/wine-shop.json').then((res)=>{
        return res.json();
    })
        .then((post)=>{
            //post array with items
            winedata = post;
            for (let index=0; index < winedata.length; index++){
                div.innerHTML+=`
                      
                     <div class="card col-sm-3" style="width: 18rem;">
                          <br>
          <img class="card-img-top" style="width: 100%; height: 150px; object-fit: contain; margin: auto; justify-content: center;" src="https://storage.googleapis.com/wineshop-assets/wine-bottles/${post[con].image}">
                        <hr style="margin-right: 16px; margin-left: 16px;">
                          <div class="card-body">
                                 ${post[con].no}<br>
                                <h5>${post[con].name}</h5>
                               Bottle Price:  $<span>${post[con].cost.bottle}</span>
                               <br>
                               Case Price:  $<span>${post[con].cost.case}</span>
                         
                          </div>
                          <div class="card-footer" style="margin-bottom: 5px;">
                            
                             <button class="btn btn-light btn-sm" onclick="prodDetails()">Details</button>
                             <button class="btn btn-primary btn-sm addToCart" onclick="prodPrice(${post[con].cost.bottle})">Add to Cart</button>
                          </div>
                                        </div>
                     
                `
                con=con+1;



            }
        }).catch((error)=>{
            console.log(error);
    })
}
function prodPrice($price){
     alert("Price of bottle : " + $price);
     window.location = 'public/checkoutform.html';
}
function prodDetails(){
    window.open('public/details.html');
}

